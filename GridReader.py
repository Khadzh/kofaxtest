#!/usr/bin/env python3
import copy

class Grid:
    def __init__(self, column_number, row_number, cell_size):
        self.column_number = column_number
        self.row_number = row_number
        self.cell_size = cell_size
        self.grid_by_rows = self.generateGrid();
        self.printGrid()

    def generateGrid(self):
        grid_by_rows = []
        for i in range(0,self.row_number):
            row = []
            for j in range(0,self.column_number):
                row.append(0)

            grid_by_rows.append(row)
        return grid_by_rows

    def printGrid(self):
        for row in self.grid_by_rows[::-1]:
            for c in row:
                print(c, end = " ")
            print()
        print()

    def toggleByRectangle(self,leftDownX,leftDownY,width,height):
        prev_grid = copy.deepcopy(self.grid_by_rows)
        rightUpX = leftDownX+width
        rightUpY = leftDownY+height
        first_row = int(leftDownY/self.cell_size) if leftDownY >= 0 else 0
        first_column = int(leftDownX/self.cell_size) if leftDownX >= 0 else 0
        last_row = int( rightUpY/self.cell_size - 1 if (rightUpY%self.cell_size == 0) else rightUpY/self.cell_size)
        last_column = int(rightUpX / self.cell_size - 1 if rightUpX % self.cell_size == 0 else rightUpX / self.cell_size)
        if(last_row >= 0 and last_column >= 0):
            for i in range(first_row,last_row+1):
                for j in range(first_column, last_column+1):
                    if(i >= self.row_number or j >= self.column_number):
                        break
                    self.grid_by_rows[i][j] = self.toggleCell(i,j)
        self.printGrid()
        print(self.getLastRow(prev_grid), "\n\n")


    def toggleCell(self,i,j):
        return 1 if self.grid_by_rows[i][j] == 0 else 0
    def getLastRow(self,prev_grid):
        row = []
        for c in range(0, self.column_number):
            column_sum = 0
            for r in range(0,self.row_number):
                m = self.grid_by_rows[r][c]*prev_grid[r][c]
                column_sum += m
            row.append(column_sum)
        return row





def main():
    g = Grid(10, 10, 10)
    print("Input your rectangle left corner coordinates, "
          "width and height in the following order: X Y width height."
          "\nIf you want to quit the program type 'Exit'")
    while(True):
        inp = input("Input X Y width height: ").lower()
        if(inp == "exit"):
            exit()
        args = [int(s) for s in inp.strip().split(" ")]
        if(len(args) < 4):
            print("Insufficient arguments!")
            break
        g.toggleByRectangle(args[0],args[1],args[2],args[3])
if __name__ == "__main__":
    main()